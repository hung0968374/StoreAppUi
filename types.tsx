export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type StackParamList = {
  TopNav: undefined;
  BottomNav: undefined;
}

export type BottomTabParamList = {
  Home: undefined;
  Categories: undefined;
  News: undefined;
  Chat: undefined;
  Account: undefined;
  TopNavigator: undefined;
};

export type TopTabParamList = {
  SearchScreen: undefined;
  FilterScreen: undefined;
  NotificationsScreen: undefined;
  CartScreen: undefined;
}

export type HomeTabParamList = {
  HomeScreen: undefined;
  ItemDetailScreen: undefined;
};

export type CategoriesTabParamList = {
  CategoriesScreen: undefined;
  CategoryScreen: undefined;
  ItemDetailScreen: undefined;
};

export type NewsTabParamList = {
  NewsScreen: undefined;
  NewsDetailScreen: undefined;
};

export type AccountTabParamList = {
  AccountScreen: undefined;
  InfoScreen: undefined;
  ReviewsScreen: undefined;
  LoginScreen: undefined;
  OrderScreen: undefined;
};
